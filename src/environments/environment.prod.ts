export const environment = {
  production: true,
  apiUrl: 'https://apis.brskly.co/',
  aws_mobile_bucket_path: 'brskly-mobile-app',
  aws_bucket_path: 'brskly-app'
};
