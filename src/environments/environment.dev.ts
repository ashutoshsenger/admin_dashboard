export const environment = {
  production: false,
  apiUrl: 'https://devapis.brskly.co/',
  aws_mobile_bucket_path: 'brskly-mobile-app-dev',
  aws_bucket_path: 'brskly-app-dev'
};
