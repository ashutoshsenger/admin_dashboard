import { Component, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar, MatSort, MatTableDataSource, MatOption} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { AppGlobals } from '../services/app-globals';
import { LoaderService } from '../services/loader.service';
import { SpaceService } from '../services/space.service';
import {GlobalVariables} from '../global/global-variables';
import * as moment from 'moment';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

export interface Space {
  s_no: number;
  alternate_id: string;
  entity_alternate_id: string;
  total_amount: number;
  entity_name: string;
  entity_type: number;
  status: number;
  paid_status: number;
  send_status: number;
  tracked_with_bhaifi: number;
}

@Component({
  selector: 'app-spaces',
  templateUrl: './spaces.component.html',
  styleUrls: ['./spaces.component.css']
})
export class SpacesComponent implements OnInit {

  public selected_status: number;
  public selected_space: number;
  public list_space;
  public search_string;
  public date_display;
  public date;
  public space_details;
  constructor(
    private loaderService: LoaderService,
    private _spaceservice: SpaceService,
    public dialog: MatDialog,
    public viewContainerRef: ViewContainerRef,
    public confirm_dialog: MatDialog,
    public confirm_viewContainerRef: ViewContainerRef,
    private _appGlobals: AppGlobals,
    public snackBar: MatSnackBar
  ) {}

  dialogRef: MatDialogRef<any>
  confirm_dialogRef: MatDialogRef<any>

  private _spaces_data: Space[];
  public selected_spaces_data: Space[] = [];
  public spacesDataSource;

  selection = new SelectionModel<Space>(true, []);
  spacesDisplayedColumns: string[] = ['id', 'name', 'status', 'area_name', 'about', 'address', 'lat', 'longi' ];

  @ViewChild('allSelected') private allSelected: MatOption;

  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.selected_status = 0;
    this.populateSpaces();
    
  }

  public populateSpaces () {
    this._spaceservice.getAll()
    .then(res => {
      if (res.success) {
        this._spaces_data = Object.assign([], res.data);
        this.selected_spaces_data = this._spaces_data;
        this.spacesDataSource = new MatTableDataSource(this.selected_spaces_data)
        this.spacesDataSource.sort = this.sort;
      } else {
        alert(res.message || "Some error occured");
      }
    })
    .catch(error=> {
      console.log(error);
    })
  }

  public changeStatus(status: number) {
    this.selected_status = status;
    switch (status) {
      case 0:
        this.selected_spaces_data = Object.assign([], this._spaces_data);
        break;
      case 1:
        this.selected_spaces_data = Object.assign([], _.filter(this._spaces_data, ['status', 1]));
        break;
      case 2:
        this.selected_spaces_data = Object.assign([], _.filter(this._spaces_data, ['status', 2]));
        break;
      default:
        this.selected_spaces_data = Object.assign([], this._spaces_data);
    }
      this.spacesDataSource = new MatTableDataSource(this.selected_spaces_data)
  }

  private _openSnackBar(message: string, action: string) {
    this.loaderService.displayLoader(false);
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  applyFilter(filterValue: string) {
    this.spacesDataSource.filter = filterValue.trim().toLowerCase();
  }

  public clearSearchInput () {
    this.search_string = '';
    this.applyFilter('');
  }

}