import { Component, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar, MatSort, MatTableDataSource, MatOption} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { AppGlobals } from '../services/app-globals';
import { LoaderService } from '../services/loader.service';
import { PlanService } from '../services/plan.service';
import {GlobalVariables} from '../global/global-variables';
import * as moment from 'moment';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

export interface Plan {
  s_no: number;
  alternate_id: string;
  entity_alternate_id: string;
  total_amount: number;
  entity_name: string;
  entity_type: number;
  status: number;
  paid_status: number;
  send_status: number;
  tracked_with_bhaifi: number;
}

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.css']
})
export class PlansComponent implements OnInit {
  public selected_status: number;
  public selected_plan: number;
  public list_plan;
  public search_string;
  public date_display;
  public date;
  public plan_details;
  constructor(
    private loaderService: LoaderService,
    private _planservice: PlanService,
    public dialog: MatDialog,
    public viewContainerRef: ViewContainerRef,
    public confirm_dialog: MatDialog,
    public confirm_viewContainerRef: ViewContainerRef,
    private _appGlobals: AppGlobals,
    public snackBar: MatSnackBar
  ) {}

  dialogRef: MatDialogRef<any>
  confirm_dialogRef: MatDialogRef<any>

  private _plans_data: Plan[];
  public selected_plans_data: Plan[] = [];
  public plansDataSource;

  selection = new SelectionModel<Plan>(true, []);
  plansDisplayedColumns: string[] = ['id', 'name', 'amount', 'credits', 'visits', 'priority'];

  @ViewChild('allSelected') private allSelected: MatOption;

  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.selected_status = 0;
    this.populatePlans();
    
  }

  public populatePlans () {
    this._planservice.getAll()
    .then(res => {
      if (res.success) {
        this._plans_data = Object.assign([], res.data);
        this.selected_plans_data = this._plans_data;
        this.plansDataSource = new MatTableDataSource(this.selected_plans_data)
        this.plansDataSource.sort = this.sort;
      } else {
        alert(res.message || "Some error occured");
      }
    })
    .catch(error=> {
      console.log(error);
    })
  }

  public changeStatus(status: number) {
    this.selected_status = status;
    switch (status) {
      case 0:
        this.selected_plans_data = Object.assign([], this._plans_data);
        break;
      case 1:
        this.selected_plans_data = Object.assign([], _.filter(this._plans_data, ['status', 1]));
        break;
      case 2:
        this.selected_plans_data = Object.assign([], _.filter(this._plans_data, ['status', 2]));
        break;
      default:
        this.selected_plans_data = Object.assign([], this._plans_data);
    }
      this.plansDataSource = new MatTableDataSource(this.selected_plans_data)
  }

  private _openSnackBar(message: string, action: string) {
    this.loaderService.displayLoader(false);
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  applyFilter(filterValue: string) {
    this.plansDataSource.filter = filterValue.trim().toLowerCase();
  }

  public clearSearchInput () {
    this.search_string = '';
    this.applyFilter('');
  }

}