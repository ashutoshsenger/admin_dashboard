import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { ChartsModule } from 'ng2-charts';
import { RouterModule } from '@angular/router';
import { MaterialModule} from './material.module';
import { AuthenticationService } from './services/authentication.service';
import { CompanyService } from './services/company.service';
import { ReceiptService } from './services/receipt.service';
import { OverviewService } from './services/overview.service';
import { WorkerService } from './services/worker.service';
import { MembershipService} from './services/membership.service';
import { PlanService } from './services/plan.service';
import { TransactionService } from './services/transaction.service';
import { RevenueService} from './services/revenue.service';
import { ManagerService } from './services/manager.service';
import { ResourceService } from './services/resource.service';
import { MeetingRoomService } from './services/meetingRoom.service';
import { FileUploadModule } from 'ng2-file-upload';
import { MemberService} from './services/member.service';
import {MatCheckboxModule} from '@angular/material';
import { BookingService} from './services/booking.service';
import {LocationListBottomSheet} from './home/location-list-bottom-sheet.component';
import { PerformaInvoiceService } from './services/performaInvoice.service';
import { InvoiceService } from './services/invoice.service';
import { ContractService} from './services/contract.service';
import { RecurringInvoiceService } from './services/recurringInvoice.service';
import { ConversationService } from './services/conversation.service';
import { SpaceService } from './services/space.service';
import { AppGlobals } from './services/app-globals';
import {DateSuffix} from './pipes/date-suffix.pipe';
import { HelpdeskService } from './services/helpdesk.service';
import { LoaderService } from './services/loader.service';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { routes, navigatableComponents } from './app-routing.module';
import { VisitorService } from './services/visitor.service';
import { LoaderInterceptorService } from './services/loader-interceptor.service';
import { BookingsComponent } from './bookings/bookings.component';
import { SpacesComponent } from './spaces/spaces.component';
import { WorkersComponent } from './workers/workers.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { PlansComponent } from './plans/plans.component';
import { QueriesComponent } from './queries/queries.component';
import { LogoutDialog } from './home/logout-dialog.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    FileUploadModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    MaterialModule,
    MatCheckboxModule,
    ChartsModule,
    RouterModule.forRoot(routes,  { useHash: true })
  ],
  declarations: [
    AppComponent,
    LocationListBottomSheet,
    LogoutDialog,
    DateSuffix,
    ...navigatableComponents,
    BookingsComponent,
    SpacesComponent,
    WorkersComponent,
    TransactionsComponent,
    PlansComponent,
    QueriesComponent
  ],
  entryComponents: [
    LogoutDialog,
    LocationListBottomSheet
  ],
  providers: [
    AuthenticationService,
    LoaderService,
    CompanyService,
    MemberService,
    WorkerService,
    MembershipService,
    PerformaInvoiceService,
    InvoiceService,
    RecurringInvoiceService,
    TransactionService,
    SpaceService,
    BookingService,
    ReceiptService,
    AppGlobals,
    OverviewService,
    VisitorService,
    HelpdeskService,
    ConversationService,
    ResourceService,
    PlanService,
    ManagerService,
    MeetingRoomService,
    RevenueService,
    ContractService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    }
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
