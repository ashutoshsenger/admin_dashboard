export const GlobalVariables = {
  billing_types: [
    {
      name:  "month-to-month", id: 1
    }, {
      name:  "fixed", id: 2
    }
  ],

  hsn_list: [
    {id: 1, value: 997212, tax_rate: 18}
  ],

  resource_types: [
    {
      name: "Dedicated Desk",
      id: 1
    }, {
      name: "Hot Desk",
      id: 4
    }, {
      name: "Cabin",
      id: 3
    }, {
      name: "Others",
      id: 5
    }],

  payment_methods: [
    {
      name: "Cheque", id: 1
    }, {
      name: "Cash", id: 2
    }, {
      name: "Online (Netbanking)", id: 3
    },
    {
      name: "Online (Payment gateway)", id: 4
    },{
      name: "Others", id: 5
    }
  ],

  facilities: [
    {id: 1, name: "Projector"},
    {id: 2, name: "Adapters"},
    {id: 3, name: "Connectors"},
    {id: 4, name: "Conference Call Phone"},
    {id: 5, name: "Display Screen"},
    {id: 6, name: "Large Monitor"},
    {id: 7, name: "Whiteboard"},
    {id: 8, name: "Fresh Markers"},
    {id: 9, name: "Power Outlets"},
    {id: 10, name: "Tea/Coffee"},
    {id: 11, name: "Wifi"},
    {id: 12, name: "Wired Internet Connection"},
    {id: 13, name: "Noise Free"},
    {id: 14, name: "Speakers"}
  ],

  modules: [
    {
      name: "Billing",
      id: 1,
      route: "billing",
    }, {
      name: "Community",
      id: 2,
      route: "community"
    }, {
      name: "Overview",
      id: 3,
      route: "overview"
    }, {
      name: "Instant Bookings",
      id: 4,
      route: "bookings"
    }, {
      name: "Helpdesk",
      id: 5,
      route: "issues"
    }, {
      name: "Conversations",
      id: 6,
      route: "conversations"
    }, {
      name: "Settings",
      id: 7,
      route: "settings"
    }],

  period: [
    {
      name: "April", id: 4 
    },
    {
      name: "May", id: 5
    },
    {
      name: "June", id: 6
    },
    {
      name: "July", id: 7
    },
    {
      name: "Aug", id: 8
    },
    {
      name: "Sept", id: 9
    }, 
    {
      name: "October", id: 10
    },
    {
      name: "November", id: 11
    },
    {
      name: "December", id: 12
    },
    {
      name: "Jan", id: 1
    },
    {
      name: "Feb", id: 2
    },
    {
      name: "March", id: 3
    }
  ],

  billing_dates: [
    {
      name: "1st", id: 1
    }, {
      name: "2nd", id: 2
    }, {
      name: "3rd", id: 3
    }, {
      name: "4th", id: 4
    }, {
      name: "5th", id: 5
    }, {
      name: "6th", id: 6
    }, {
      name: "7th", id: 7
    }, {
      name: "8th", id: 8
    }, {
      name: "9th", id: 9
    }, {
      name: "10th", id: 10
    }, {
      name: "11th", id: 11
    }, {
      name: "12th", id: 12
    }, {
      name: "13th", id: 13
    }, {
      name: "14th", id: 14
    }, {
      name: "15th", id: 15
    }, {
      name: "16th", id: 16
    }, {
      name: "17th", id: 17
    }, {
      name: "18th", id: 18
    }, {
      name: "19th", id: 19
    }, {
      name: "20th", id: 20
    }, {
      name: "21st", id: 21
    }, {
      name: "22nd", id: 22
    }, {
      name: "23th", id: 23
    }, {
      name: "24th", id: 24
    }, {
      name: "25th", id: 25
    }, {
      name: "26th", id: 26
    }, {
      name: "27th", id: 27
    }, {
      name: "28th", id: 28
    }, {
      name: "29th", id: 29
    }, {
      name: "30th", id: 30
    }, {
      name: "31st", id: 31
    }
  ],

  states: [
  {
    id: 7,
    name: "Delhi"
  },
  {
    id: 6,
    name: "Haryana"
  },
  {
    id: 9,
    name: "Uttar Pradesh"
  },
  {
    id: 1,
    name: "Jammu  Kashmir"
  },
  {
    id: 19,
    name: "West Bengal"
  },
  {
    id: 2,
    name: "Himachal Pradesh"
  },
  {
    id: 20,
    name: "Jharkhand"
  },
  {
    id: 3,
    name: "Punjab"
  },
  {
    id: 21,
    name: "Orissa"
  },
  {
    id: 4,
    name: "Chandigarh"
  },
  {
    id: 22,
    name: "Chhattisgarh"
  },
  {
    id: 5,
    name: "Uttarakhand"
  },
  {
    id: 23,
    name: "Madhya Pradesh"
  },
    {
    id: 24,
    name: "Gujarat"
  },
  {
    id: 25,
    name: "Daman  Diu"
  },
  {
    id: 8,
    name: "Rajasthan"
  },
  {
    id: 26,
    name: "Dadra  Nagar Haveli"
  },
  
  {
    id: 27,
    name: "Maharashtra"
  },
  {
    id: 10,
    name: "Bihar"
  },
  {
    id: 11,
    name: "Sikkim"
  },
  {
    id: 29,
    name: "Karnataka"
  },
  {
    id: 12,
    name: "Arunachal Pradesh"
  },
  {
    id: 30,
    name: "Goa"
  },
  {
    id: 13,
    name: "Nagaland"
  },
  {
    id: 31,
    name: "Lakshadweep"
  },
  {
    id: 14,
    name: "Manipur"
  },
  {
    id: 32,
    name: "Kerala"
  },
  {
    id: 15,
    name: "Mizoram"
  },
  {
    id: 33,
    name: "Tamil Nadu"
  },
  {
    id: 16,
    name: "Tripura"
  },
  {
    id: 34,
    name: "Puducherry"
  },
  {
    id: 17,
    name: "Meghalaya"
  },
  {
    id: 35,
    name: "Andaman  Nicobar Islands"
  },
  {
    id: 18,
    name: "Assam"
  },
  {
    id: 36,
    name: "Telengana"
  },
  {
    id: 37,
    name: "Andhra Pradesh"
  }
  ],
  list_status : [
    {
    id: null,
    name : "All"
    },
    {
    id: 1,
    name : "Confirmed"
    },
    {
      id: 0,
    name : "Cancelled"
    }
  ],
  minimum_booking_hours: [
    {
      name: "0.5", hours: 0.5
    },
    {
      name: "1", hours: 1
    },
    {
      name: "1.5", hours: 2.5
    },
    {
      name: "2", hours: 2
    },
    {
      name: "2.5", hours: 2.5
    },
    {
      name: "3", hours: 3
    },
    {
      name: "3.5", hours: 3.5
    },
    {
      name: "4", hours: 4
    }
  ],
  maximum_booking_hours: [
    {
      name: "1", hours: 1
    },
    {
      name: "1.5", hours: 2.5
    },
    {
      name: "2", hours: 2
    },
    {
      name: "2.5", hours: 2.5
    },
    {
      name: "3", hours: 3
    },
    {
      name: "3.5", hours: 3.5
    },
    {
      name: "4", hours: 4
    },
    {
      name: "4.5", hours: 4.5
    },
    {
      name: "5", hours: 5
    },
    {
      name: "5.5", hours: 5.5
    },
    {
      name: "6", hours: 6
    },
    {
      name: "6.5", hours: 6.5
    },
    {
      name: "7", hours: 7
    }
  ],
};
