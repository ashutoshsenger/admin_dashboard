import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class ContractService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);

  }

  private base_url = 'api/v1/community/contract';
  private updateDetailsUrl = environment.apiUrl + this.base_url +'/update';
  private updatePaymentDetailsUrl = environment.apiUrl + this.base_url +'/updatePayment';
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private deleteDetailsUrl = environment.apiUrl + this.base_url +'/delete';
  private sendDetailsUrl = environment.apiUrl + this.base_url +'/send';
  private sendReceiptUrl = environment.apiUrl + this.base_url +'/sendReceipt';
  private getCSVDataUrl = environment.apiUrl + this.base_url +'/csvData';
  private getAllUrl = environment.apiUrl + this.base_url +'/all';
  private getPdfUrl = environment.apiUrl + this.base_url +'/download';
  private getZipUrl = environment.apiUrl + this.base_url +'/bulkDownload';
  private getProfileDetailsUrl = environment.apiUrl + this.base_url +'/details';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAll (): Promise<any> {
    let location_condition = this.selectedLocationId && this.selectedLocationId != 0 ? `?location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllUrl+ location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getProfileDetails (contract_id: string, entity_type: number ): Promise<any> {
    return this.http.get(this.getProfileDetailsUrl + '?id=' + contract_id + '&entity_type='+ entity_type,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  addDetails (details: any): Promise<any> {
    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateDetails (details: any, member_id: number): Promise<any> {
    return this.http.put(this.updateDetailsUrl + '/' + member_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updatePaymentDetails (details: any, contract_id: number): Promise<any> {
    return this.http.put(this.updatePaymentDetailsUrl + '/' + contract_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  deleteDetails (contract_id: number): Promise<any> {
    return this.http.put(this.deleteDetailsUrl + '/' + contract_id, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  sendDetailsToEntity (contract_id: number, entity_type: number): Promise<any> {
    return this.http.put(this.sendDetailsUrl + '/' + contract_id + '/' + entity_type, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  sendReceipt (receipt_details:any): Promise<any> {
    return this.http.put(this.sendReceiptUrl, receipt_details, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  exportToCSV (community_month): Promise<any> {
    return this.http.get(this.getCSVDataUrl+'?month='+community_month,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  download (id: string) {
    return this.http.get(this.getPdfUrl + "?id=" + id,
    {
      withCredentials: true,
      responseType: 'blob'
    })
  }

  bulkDownload (ids: any) {
    console.log(ids);
    return this.http.get(this.getZipUrl + "?id="+ids,
    {
      withCredentials: true,
      responseType: 'blob'
    })
  }
}
