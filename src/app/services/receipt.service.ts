import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';

@Injectable()
export class ReceiptService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);

  }

  private base_url = 'api/v1/billing/receipt';
  
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private getAllUrl = environment.apiUrl + this.base_url +'/all';
  private getAllByEntityUrl = environment.apiUrl + this.base_url +'/getallByEntity';
  private sendReceiptUrl = environment.apiUrl + this.base_url +'/sendReceipt';
  private getPdfUrl = environment.apiUrl + this.base_url +'/download';
  private deleteDetailsUrl = environment.apiUrl + this.base_url +'/delete';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  addDetails (details: any): Promise<any> {
    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getAll (billing_month): Promise<any> {
    let location_condition = this.selectedLocationId && this.selectedLocationId != 0 ? `&location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllUrl+'?month='+billing_month + location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getAllByEntity (entity_id, entity_type): Promise<any> {
    return this.http.get(this.getAllByEntityUrl+ '?entity_id='+entity_id + '&entity_type=' + 1, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  download (id: string) {
    return this.http.get(this.getPdfUrl + "/" + id,
    {
      withCredentials: true,
      responseType: 'blob'
    })
  }

  deleteDetails (receipt_id: number): Promise<any> {
    return this.http.delete(this.deleteDetailsUrl + '/' + receipt_id, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
}
