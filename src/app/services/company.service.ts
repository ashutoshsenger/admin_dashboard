import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class CompanyService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => {
      this.selectedLocationId = id
    });
  }

  private base_url = 'api/v1/community/company';
  private updateDetailsUrl = environment.apiUrl + this.base_url +'/update';
  private deleteDetailsUrl = environment.apiUrl + this.base_url +'/delete';
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private getAllUrl = environment.apiUrl + this.base_url +'/all';
  private getListUrl = environment.apiUrl + this.base_url +'/list';
  private getProfileDetailsUrl = environment.apiUrl + this.base_url +'/profileDetails';
  private getMembersUrl = environment.apiUrl + this.base_url +'/members';
  private getPersonalDetailsUrl = environment.apiUrl + this.base_url +'/personalDetails';
  private getMembershipDetailsUrl = environment.apiUrl + this.base_url +'/membershipDetails';
  private getPerformaInvoiceDetailsUrl = environment.apiUrl + this.base_url +'/performaInvoiceDetails';
  private getInvoiceDetailsUrl = environment.apiUrl + this.base_url +'/invoiceDetails';
  private getContractDetailsUrl = environment.apiUrl + this.base_url +'/contractDetails';


  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAll (): Promise<any> {
    let location_condition = this.selectedLocationId && this.selectedLocationId != 0 ? `?location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllUrl+location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getList (): Promise<any> {

    let location_condition = this.selectedLocationId && this.selectedLocationId != 0 ? `?location_id=${this.selectedLocationId}`: '';
    return this.http.get(this.getListUrl + location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };

  getProfileDetails (company_id: string): Promise<any> {
    return this.http.get(this.getProfileDetailsUrl + '/' + company_id,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getPersonalDetails (company_id: string): Promise<any> {
    return this.http.get(this.getPersonalDetailsUrl + '/' + company_id,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };

  getMembers (company_id: number): Promise<any> {
    return this.http.get(this.getMembersUrl + '/' + company_id,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };

  getMembershipDetails (company_id: number): Promise<any> {
    return this.http.get(this.getMembershipDetailsUrl + '/' + company_id,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };

  getInvoiceDetails (company_id: number): Promise<any> {
    return this.http.get(this.getInvoiceDetailsUrl + '/' + company_id,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };

  getPerformaInvoiceDetails (company_id: number): Promise<any> {
    return this.http.get(this.getPerformaInvoiceDetailsUrl + '/' + company_id,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };

  addDetails (details: any): Promise<any> {
    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateDetails (details: any, company_id: string): Promise<any> {
    return this.http.put(this.updateDetailsUrl + '/' + company_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  deleteDetails (company_id: string): Promise<any> {
    return this.http.put(this.deleteDetailsUrl+ '/' + company_id, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getContractDetails (company_id: number): Promise<any> {
    return this.http.get(this.getContractDetailsUrl + '/' + company_id,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };
}
