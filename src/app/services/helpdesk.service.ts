import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class HelpdeskService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);
  }

  private base_url = 'api/v1/issue';
  private getAllIssuesUrl= environment.apiUrl + this.base_url +'/all?type=';
  private getAllCategoriesUrl = environment.apiUrl + 'api/v1/common/categories';
  private updateIssueUrl = environment.apiUrl + this.base_url + '/update';
  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAllIssues (type): Promise<any> {
    let location_condition = this.selectedLocationId != 0 ? `&location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllIssuesUrl + type+location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
  
  updateIssue (issue_id, issue_data): Promise<any> {
    return this.http.put(this.updateIssueUrl+ '/' + issue_id, JSON.stringify(issue_data), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getAllCategories (): Promise<any> {
    return this.http.get(this.getAllCategoriesUrl,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
}
