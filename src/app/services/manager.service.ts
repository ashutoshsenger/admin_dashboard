import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class ManagerService {

  public _selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this._selectedLocationId = id);
  }

  private base_url = 'api/v1/spaceManager';
  private getAllDetailsUrl = environment.apiUrl + this.base_url +'/allDetails';
  private getAllResourcesUrl = environment.apiUrl + this.base_url +'/allResources';
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private deleteDetailsUrl = environment.apiUrl + this.base_url +'/delete';
  private updateDetailsUrl = environment.apiUrl + this.base_url +'/update';
  private switchAccountUrl = environment.apiUrl + 'api/v1/space/switchAccount/';
  private getPlansListUrl = environment.apiUrl + 'api/v1/space/plansList';
  private getBasicDetailsUrl = environment.apiUrl + this.base_url + '/basicDetails';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAllDetails (location_id): Promise<any> {
    let location_condition = location_id && location_id != 0 ? `?location_id=${location_id}`: '';

    return this.http.get(this.getAllDetailsUrl + location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  addDetails (details): Promise<any> {
    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateDetails (manager_id, manager_details): Promise<any> {
    return this.http.put(this.updateDetailsUrl + '/' + manager_id, JSON.stringify(manager_details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  deleteDetails (manager_id): Promise<any> {
    return this.http.put(this.deleteDetailsUrl + '/' + manager_id, '', this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getBasicDetails (): Promise<any> {
    return this.http.get(this.getBasicDetailsUrl,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getPlansList (): Promise<any> {
   console.log (this._selectedLocationId);
    let location_condition = this._selectedLocationId && this._selectedLocationId != 0 ? `?location_id=${this._selectedLocationId}`: '';

    return this.http.get(this.getPlansListUrl+location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };

  setSelectedAccountCookie (account_id): Promise <any> {
    return this.http.get(this.switchAccountUrl + account_id, {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
}
