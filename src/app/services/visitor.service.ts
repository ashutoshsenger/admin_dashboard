import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class VisitorService {
  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);

  }

  private base_url = 'api/v1/visitor_admin';
  private updateDetailsUrl = environment.apiUrl + this.base_url +'/update';
  private deleteDetailsUrl = environment.apiUrl + this.base_url +'/delete';
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private getAllUrl = environment.apiUrl + this.base_url +'/all';
  private getAllUnInvoicedUrl = environment.apiUrl + this.base_url + '/uninvoiced';
  private searchByMobileUrl = environment.apiUrl + this.base_url +'/searchByMobile';
  private addTrackingUrl = environment.apiUrl + this.base_url +'/addTracking';
  private sendNotificationsUrl = environment.apiUrl + this.base_url +'/notifyAll';
  private getAllMembersWithCompanyUrl = environment.apiUrl + this.base_url +'/allMembers';
  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAllWithDetails (date: any): Promise<any> {
    let location_condition = (this.selectedLocationId != null && this.selectedLocationId) ? `&location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllUrl+`?date=${date}` + location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  addDetails (details: any): Promise<any> {
    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateDetails (details: any, visitor_id: number): Promise<any> {
    return this.http.put(this.updateDetailsUrl + '/' + visitor_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  deleteDetails (visitor_id: number): Promise<any> {
    return this.http.put(this.deleteDetailsUrl+ '/' + visitor_id, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  searchByMobile (mobile, otp, is_checked): Promise<any> {
    return this.http.get(this.searchByMobileUrl + '/' + mobile + `${otp ? '?otp='+ otp + '&is_checked='+ is_checked: '?is_checked=' + is_checked}`,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };

  addTracking (details: any): Promise<any> {
    return this.http.post(this.addTrackingUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => {
      return res
    })
    .catch(this.handleError);
  };

  sendNotifications (id: number): Promise<any> {
    return this.http.post(this.sendNotificationsUrl, JSON.stringify({id}), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getAllMembersWithCompany (): Promise<any> {
    let location_condition = this.selectedLocationId != 0 ? `?location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllMembersWithCompanyUrl+location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

}
