import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';

@Injectable()
export class SpaceService {

  private _selectedLocationId;

  constructor (
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this._selectedLocationId = id);
}

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  private base_url = 'api/v1/admin';

  private getAllUrl = environment.apiUrl + this.base_url +'/spaces';

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  };

  private _options = {
    headers: this.headers,
    withCredentials: true
  };

  getAll (): Promise<any> {

    return this.http.get(this.getAllUrl,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

}
