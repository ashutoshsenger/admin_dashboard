import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class RevenueService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);
  }

  private base_url = 'api/v1/overview/revenue';
  private getGeneralDetailsUrl = environment.apiUrl + this.base_url +'/details';

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getGeneralDetails (month_year_1, month_year_2, month_year_3): Promise<any> {

    let location_condition = this.selectedLocationId && this.selectedLocationId  != 0 ? `&location_id=${this.selectedLocationId}`: '';
    return this.http.get(this.getGeneralDetailsUrl+`?month_1=${month_year_1}&month_2=${month_year_2}&month_3=${month_year_3}` + location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  };
}
 
