import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class RecurringInvoiceService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);

  }

  private base_url = 'api/v1/billing/recurringInvoice';
  private updateDetailsUrl = environment.apiUrl + this.base_url +'/update';
  private updatePaymentDetailsUrl = environment.apiUrl + this.base_url +'/updatePayment';
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private deleteDetailsUrl = environment.apiUrl + this.base_url +'/delete';
  private sendDetailsUrl = environment.apiUrl + this.base_url +'/send';
  private sendReceiptUrl = environment.apiUrl + this.base_url +'/sendReceipt';
  private getCSVDataUrl = environment.apiUrl + this.base_url +'/csvData';
  private getAllUrl = environment.apiUrl + this.base_url +'/all';
  private getPdfUrl = environment.apiUrl + this.base_url +'/download';
  private getProfileDetailsUrl = environment.apiUrl + this.base_url +'/details';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAll (): Promise<any> {
    let location_condition = this.selectedLocationId && this.selectedLocationId != 0 ? `?location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllUrl+ location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getProfileDetails (recurringInvoice_id: string, entity_type: number ): Promise<any> {
    return this.http.get(this.getProfileDetailsUrl + '?id=' + recurringInvoice_id + '&entity_type='+ entity_type,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  addDetails (details: any): Promise<any> {
    details.location_id = this.selectedLocationId ? this.selectedLocationId : 0;

    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateDetails (details: any, alternate_id: string): Promise<any> {
    return this.http.put(this.updateDetailsUrl + '/' + alternate_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updatePaymentDetails (details: any, recurringInvoice_id: number): Promise<any> {
    return this.http.put(this.updatePaymentDetailsUrl + '/' + recurringInvoice_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  deleteDetails (recurringInvoice_id: number): Promise<any> {
    return this.http.put(this.deleteDetailsUrl + '/' + recurringInvoice_id, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  sendDetailsToEntity (recurringInvoice_id: number, entity_type: number): Promise<any> {
    return this.http.put(this.sendDetailsUrl + '/' + recurringInvoice_id + '/' + entity_type, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  sendReceipt (receipt_details:any): Promise<any> {
    return this.http.put(this.sendReceiptUrl, receipt_details, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  exportToCSV (billing_month): Promise<any> {
    return this.http.get(this.getCSVDataUrl+'?month='+billing_month,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  download (id: string) {
    return this.http.get(this.getPdfUrl + "/" + id,
    {
      withCredentials: true,
      responseType: 'blob'
    })
  }
}
