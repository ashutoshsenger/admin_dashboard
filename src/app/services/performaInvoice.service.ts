import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class PerformaInvoiceService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);

  }

  private base_url = 'api/v1/billing/performaInvoice';
  private updateDetailsUrl = environment.apiUrl + this.base_url +'/update';
  private updatePaymentDetailsUrl = environment.apiUrl + this.base_url +'/updatePayment';
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private sendDetailsForTestingUrl= environment.apiUrl + this.base_url +'/sendForTesting';
  private deleteDetailsUrl = environment.apiUrl + this.base_url +'/delete';
  private sendDetailsUrl = environment.apiUrl + this.base_url +'/send';
  private sendReceiptUrl = environment.apiUrl + this.base_url +'/sendReceipt';
  private getCSVDataUrl = environment.apiUrl + this.base_url +'/csvData';
  private getAllUrl = environment.apiUrl + this.base_url +'/all';
  private getPdfUrl = environment.apiUrl + this.base_url +'/download';
  private getProfileDetailsUrl = environment.apiUrl + this.base_url +'/details';
  private convertToInvoiceUrl = environment.apiUrl + this.base_url +'/convertToInvoice';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAll (start_date:string, end_date:string): Promise<any> {
    let location_condition = this.selectedLocationId && this.selectedLocationId != 0 ? `&location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllUrl+'?start_date='+start_date + '&end_date='+end_date+location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getProfileDetails (performaInvoice_id: string, entity_type: number ): Promise<any> {
    return this.http.get(this.getProfileDetailsUrl + '?id=' + performaInvoice_id + '&entity_type='+ entity_type,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  addDetails (details: any): Promise<any> {

    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateDetails (details: any, member_id: number): Promise<any> {
    return this.http.put(this.updateDetailsUrl + '/' + member_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  convertToInvoice (alternate_id: string, issue_date:string, due_date: string): Promise<any> {
    return this.http.put(this.convertToInvoiceUrl + '/' + alternate_id + '?issue_date='+issue_date+'&due_date='+due_date, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updatePaymentDetails (details: any, performaInvoice_id: number): Promise<any> {
    return this.http.put(this.updatePaymentDetailsUrl + '/' + performaInvoice_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  deleteDetails (performaInvoice_id: number): Promise<any> {
    return this.http.put(this.deleteDetailsUrl + '/' + performaInvoice_id, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  sendDetailsToEntity (performaInvoice_id: number, entity_type: number): Promise<any> {
    return this.http.put(this.sendDetailsUrl + '/' + performaInvoice_id + '/' + entity_type, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  exportToCSV (billing_month): Promise<any> {
    return this.http.get(this.getCSVDataUrl+'?month='+billing_month,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  download (id: string) {
    return this.http.get(this.getPdfUrl + "/" + id,
    {
      withCredentials: true,
      responseType: 'blob'
    })
  }

  sendDetailsToEntityForTesting (performaInvoice_id: number, entity_type: number, email: string): Promise<any> {
    return this.http.put(this.sendDetailsForTestingUrl + '/' + performaInvoice_id + '/' + entity_type + '?email='+email, {}, this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
}
