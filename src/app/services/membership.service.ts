import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class MembershipService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);

  }

  private base_url = 'api/v1/community/membership';
  private updateDetailsUrl = environment.apiUrl + this.base_url +'/update';
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private getAllUrl = environment.apiUrl + this.base_url +'/all';
  private getCSVDataUrl = environment.apiUrl + this.base_url +'/csvData';
  private getAllUnInvoicedUrl = environment.apiUrl + this.base_url + '/uninvoiced';
  private getAllUnexpiredUrl = environment.apiUrl + this.base_url + '/allUnexpired';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAllWithDetails (): Promise<any> {
    let location_condition = this.selectedLocationId != 0 ? `?location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllUrl+location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  addDetails (details: any): Promise<any> {
    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getAllUnInvoiced (entity_type, entity_id, start, end, invoice_type): Promise<any> {
    let entity_condition = `?company_id=${entity_type == 1 ? entity_id:''}&member_id=${entity_type == 2 ? entity_id:''}&start=${start}&end=${end}&invoice_type=${invoice_type}`;

    return this.http.get(this.getAllUnInvoicedUrl + entity_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateDetails (details: any, membership_id: number): Promise<any> {
    return this.http.put(this.updateDetailsUrl + '/' + membership_id, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  exportToCSV (): Promise<any> {
    let location_condition = this.selectedLocationId != 0 ? `?location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getCSVDataUrl+location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getAllUnexpired (entity_type, entity_id, location_id): Promise<any> {

    return this.http.get(this.getAllUnexpiredUrl+ `?entity_type=${entity_type}&entity_id=${entity_id}&location_id=${location_id}`,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
}
