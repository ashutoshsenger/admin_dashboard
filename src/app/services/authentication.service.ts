import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {

constructor (private http: HttpClient) {}

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  private base_url = 'api/v1/user';
  private validateUserUrl = environment.apiUrl + this.base_url + '/login';
  private validateSessionUrl = environment.apiUrl + this.base_url + '/validateSession';

  private forgotPasswordUrl = environment.apiUrl + 'api/v1/app/user/forgotPassword';
  private resetPasswordUrl = environment.apiUrl + 'api/v1/app/user/resetPassword';
  private logoutUrl = environment.apiUrl + this.base_url + '/logout';

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  };

  private options = {
    headers: this.headers, 
    withCredentials: true
  };

  login (email: string, password: string): Promise<any> {

    return  this.http.post(this.validateUserUrl, JSON.stringify ({'email': email, 'password': password, 'type': 3}), this.options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  validateSession (): Promise<any> {
    return  this.http.get(this.validateSessionUrl, this.options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  deletCookies() {
    return  this.http.put(this.logoutUrl, {}, this.options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  forgotPassword (email: string, type: number): Promise<any> {

    return  this.http.post(this.forgotPasswordUrl, JSON.stringify ({'email': email, type}), this.options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  resetPassword (alternate_id: string, password: string): Promise<any> {

    return  this.http.put(this.resetPasswordUrl, JSON.stringify ({alternate_id, password, type: 1}), this.options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
}
