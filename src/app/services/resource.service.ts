import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../../environments/environment';
import { AppGlobals } from './app-globals';


@Injectable()
export class ResourceService {

  public selectedLocationId;

  constructor(
    private http: HttpClient,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.selectedLocationId.subscribe(id => this.selectedLocationId = id);
  }

  private base_url = 'api/v1/settings/resource';
  private getAllUrl = environment.apiUrl + this.base_url +'/all';
  private getAllDetailsUrl = environment.apiUrl + this.base_url +'/allDetails';
  private addDetailsUrl = environment.apiUrl + this.base_url +'/add';
  private updateDetailsUrl = environment.apiUrl + this.base_url +'/update';
  private deleteDetailsUrl = environment.apiUrl + this.base_url +'/delete';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private _options = {
    headers: this.headers, 
    withCredentials: true
  };

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getAll (resource_types): Promise<any> {
    let location_condition = this.selectedLocationId != 0 ? `&location_id=${this.selectedLocationId}`: '';
    return this.http.get(this.getAllUrl + '?type=' + resource_types + location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getAllDetails (resource_types): Promise<any> {
    let location_condition = this.selectedLocationId != 0 ? `&location_id=${this.selectedLocationId}`: '';
    return this.http.get(this.getAllDetailsUrl + '?type=' + resource_types + location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
 
  addDetails (details): Promise<any> {
    return this.http.post(this.addDetailsUrl, JSON.stringify(details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateDetails (resource_id, resource_details): Promise<any> {
    return this.http.put(this.updateDetailsUrl + '/' + resource_id, JSON.stringify(resource_details), this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  getAllResources (type_array): Promise<any> {
    let location_condition = this.selectedLocationId != 0 ? `?location_id=${this.selectedLocationId}`: '';

    return this.http.get(this.getAllUrl + '/' + type_array + location_condition,
    {
      withCredentials: true
    })
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  deleteDetails (resource_id): Promise<any> {
    return this.http.put(this.deleteDetailsUrl + '/' + resource_id, '', this._options)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
}
