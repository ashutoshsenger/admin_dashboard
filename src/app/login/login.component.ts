import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { User } from './user';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  user: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.user = new User;
  }

  public show_forgot_password = false;
  public alert_message = '';
  public alert_success = '';

  email = new FormControl('', [Validators.required, Validators.email]);

  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
  password = new FormControl('', [Validators.required]);

  getPasswordErrorMessage() {
    return this.password.hasError('required') ? 'You must enter a value': '';
  }

  private _verifiySession () {
    this.authenticationService.validateSession ()
    .then(data => {
      if (data.verified) {
          this.router.navigate(['home']);
      }
    })
    .catch(error => {
      console.log(error);
    })
  }

  ngOnInit () {
    this._verifiySession();
  }

  validate() {
    this.authenticationService.login(this.user.email, this.user.password)
      .then(res => {
        if(res.success) {
          this.router.navigate(['home/bookings']);
        } else {
          this.alert_success = 'warn';
          this.alert_message = res.message;
        } 
      })
      .catch(err => console.log('err')) 
  }

  public forgotPassword () {
    this.authenticationService.forgotPassword (this.user.email, 1)
    .then(res => {
      if (res.success) {
        this.alert_success = 'success';
        window.location.reload();
      } else {
        this.alert_success = 'warn';
      }
      this.alert_message = res.message;
    })
    .catch(err => console.log('err'))

  }
}
