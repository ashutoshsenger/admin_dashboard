import { Routes } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { BookingsComponent } from './bookings/bookings.component';
import { SpacesComponent } from './spaces/spaces.component';
import { WorkersComponent } from './workers/workers.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { PlansComponent } from './plans/plans.component';
import { QueriesComponent } from './queries/queries.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';


export const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {path: '',  redirectTo: 'home', pathMatch: 'full'},
  {
    path: 'home', component: HomeComponent,
    children: [
      { path: '', redirectTo: 'bookings', pathMatch: 'full' },
      { path: 'bookings', component: BookingsComponent },
      { path: 'plans', component: PlansComponent },
      { path: 'workers', component: WorkersComponent },
      { path: 'spaces', component: SpacesComponent },
      { path: 'transactions', component: TransactionsComponent },
      { path: 'queries', component: QueriesComponent },
    ]
  }
];

export const navigatableComponents = [
  OverviewComponent,
  LoginComponent,
  HomeComponent,
  BookingsComponent,
  SpacesComponent,
  WorkersComponent,
  PlansComponent,
  QueriesComponent,
  TransactionsComponent
];
