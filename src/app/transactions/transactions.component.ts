import { Component, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar, MatSort, MatTableDataSource, MatOption} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { AppGlobals } from '../services/app-globals';
import { LoaderService } from '../services/loader.service';
import { TransactionService } from '../services/transaction.service';
import {GlobalVariables} from '../global/global-variables';
import * as moment from 'moment';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

export interface Transactions {
  s_no: number;
  alternate_id: string;
  entity_alternate_id: string;
  total_amount: number;
  entity_name: string;
  entity_type: number;
  status: number;
  paid_status: number;
  send_status: number;
  tracked_with_bhaifi: number;
}

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  public selected_status: number;
  public selected_transactions: number;
  public list_transactions;
  public search_string;
  public date_display;
  public date;
  public transactions_details;
  constructor(
    private loaderService: LoaderService,
    private _transactionsservice: TransactionService,
    public dialog: MatDialog,
    public viewContainerRef: ViewContainerRef,
    public confirm_dialog: MatDialog,
    public confirm_viewContainerRef: ViewContainerRef,
    private _appGlobals: AppGlobals,
    public snackBar: MatSnackBar
  ) {}

  dialogRef: MatDialogRef<any>
  confirm_dialogRef: MatDialogRef<any>

  private _transactionss_data: Transactions[];
  public selected_transactionss_data: Transactions[] = [];
  public transactionssDataSource;

  selection = new SelectionModel<Transactions>(true, []);
  transactionssDisplayedColumns: string[] = ['id', 'amount', 'status', 'gateway_id', 'coupon_code', 'added_at', 'visits', 'purpose' ];

  @ViewChild('allSelected') private allSelected: MatOption;

  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.selected_status = 0;
    this.populateTransactionss();
    
  }

  public populateTransactionss () {
    this._transactionsservice.getAll()
    .then(res => {
      if (res.success) {
        this._transactionss_data = Object.assign([], res.data);
        this.selected_transactionss_data = this._transactionss_data;
        this.transactionssDataSource = new MatTableDataSource(this.selected_transactionss_data)
        this.transactionssDataSource.sort = this.sort;
      } else {
        alert(res.message || "Some error occured");
      }
    })
    .catch(error=> {
      console.log(error);
    })
  }

  public changeStatus(status: number) {
    this.selected_status = status;
    switch (status) {
      case 0:
        this.selected_transactionss_data = Object.assign([], this._transactionss_data);
        break;
      case 1:
        this.selected_transactionss_data = Object.assign([], _.filter(this._transactionss_data, ['status', 1]));
        break;
      case 2:
        this.selected_transactionss_data = Object.assign([], _.filter(this._transactionss_data, ['status', 2]));
        break;
      default:
        this.selected_transactionss_data = Object.assign([], this._transactionss_data);
    }
      this.transactionssDataSource = new MatTableDataSource(this.selected_transactionss_data)
  }

  private _openSnackBar(message: string, action: string) {
    this.loaderService.displayLoader(false);
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  applyFilter(filterValue: string) {
    this.transactionssDataSource.filter = filterValue.trim().toLowerCase();
  }

  public clearSearchInput () {
    this.search_string = '';
    this.applyFilter('');
  }

}