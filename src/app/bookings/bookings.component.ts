import { Component, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar, MatSort, MatTableDataSource, MatOption} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { AppGlobals } from '../services/app-globals';
import { LoaderService } from '../services/loader.service';
import { BookingService } from '../services/booking.service';
import {GlobalVariables} from '../global/global-variables';
import * as moment from 'moment';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

export interface Booking {
  s_no: number;
  alternate_id: string;
  entity_alternate_id: string;
  total_amount: number;
  entity_name: string;
  entity_type: number;
  status: number;
  paid_status: number;
  send_status: number;
  tracked_with_bhaifi: number;
}

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})

export class BookingsComponent implements OnInit {

  public selected_status: number;
  public selected_space: number;
  public list_space;
  public search_string;
  public date_display;
  public date;
  public booking_details;
  constructor(
    private loaderService: LoaderService,
    private _bookingService: BookingService,
    public dialog: MatDialog,
    public viewContainerRef: ViewContainerRef,
    public confirm_dialog: MatDialog,
    public confirm_viewContainerRef: ViewContainerRef,
    private _appGlobals: AppGlobals,
    public snackBar: MatSnackBar
  ) {}

  dialogRef: MatDialogRef<any>
  confirm_dialogRef: MatDialogRef<any>

  private _bookings_data: Booking[];
  public selected_bookings_data: Booking[] = [];
  public bookingsDataSource;

  selection = new SelectionModel<Booking>(true, []);
  bookingsDisplayedColumns: string[] = ['booking_id', 'name', 'date', 'space_name', 'status', ];

  @ViewChild('allSelected') private allSelected: MatOption;

  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.selected_status = 0;
    this.date = moment().startOf('month').toDate();
    this.date_display = moment(this.date).format('MMM Do YYYY');
    this._bookingService.getAllSpaces()
    .then(data => {
      this.list_space = data.data;
    })
    this.populateBookings();
    
  }

  public onChange(date) {
    this.date = date || this.date;
    this.date_display = moment(this.date).format('MMM Do YYYY');
    this.populateBookings();
  }

  public changeSpace(selected_space) {
   this.populateBookings();
  }

  public populateBookings () {
    let date = moment(this.date).format('YYYY-MM-DD');
    this._bookingService.getAll(date,this.selected_space)
    .then(res => {
      if (res.success) {
        this._bookings_data = Object.assign([], res.data);
        this.selected_bookings_data = this._bookings_data;
        this.bookingsDataSource = new MatTableDataSource(this.selected_bookings_data)
        this.bookingsDataSource.sort = this.sort;
      } else {
        alert(res.message || "Some error occured");
      }
    })
    .catch(error=> {
      console.log(error);
    })
  }

  public changeStatus(status: number) {
    this.selected_status = status;
    switch (status) {
      case 0:
        this.selected_bookings_data = Object.assign([], this._bookings_data);
        break;
      case 1:
        this.selected_bookings_data = Object.assign([], _.filter(this._bookings_data, ['status', 1]));
        break;
      case 2:
        this.selected_bookings_data = Object.assign([], _.filter(this._bookings_data, ['status', 2]));
        break;
      default:
        this.selected_bookings_data = Object.assign([], this._bookings_data);
    }
      this.bookingsDataSource = new MatTableDataSource(this.selected_bookings_data)
  }

  private _openSnackBar(message: string, action: string) {
    this.loaderService.displayLoader(false);
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  applyFilter(filterValue: string) {
    this.bookingsDataSource.filter = filterValue.trim().toLowerCase();
  }

  public clearSearchInput () {
    this.search_string = '';
    this.applyFilter('');
  }

}

