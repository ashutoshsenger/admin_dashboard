import { Component } from '@angular/core';

@Component({
  selector: 'brskly-space-app',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {}
