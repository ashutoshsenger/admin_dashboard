export class SpaceConfig {
  name:string;
  open_time: string;
  close_time: string;
  is_super_admin: boolean;
  color: string;
  invoice_prefix: string;
  bhaifi_linked_id: number;
}
