import { Component, ViewContainerRef, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from '../../services/loader.service';
import 'rxjs/add/observable/of';
import {BaseChartDirective} from 'ng2-charts/ng2-charts';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'membership',
  templateUrl: './membership-overview.component.html',
  styleUrls: ['./membership-overview.component.css']
})

export class MembershipOverviewComponent implements OnInit {

  constructor(
    private loaderService: LoaderService,
    public dialog: MatDialog,
    public viewContainerRef_bookings: ViewContainerRef,
    private route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) { }
 
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

    public date = new Date().toISOString().slice(0, 10);

  public pieChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
  public pieChartData:number[] = [20000, 20000, 13500];
  public pieChartType:string = 'pie';

  public memberships: Array<{name: String, price: number, sold_count: number}>;
  
  ngOnInit() {
    this.memberships = [
      {
        name: "Dedicated desk",
        price: 5000,
        sold_count: 4
      },
      {
        name: "Private office",
        price: 5000,
        sold_count: 4
      },
      {
        name: "Hot desk",
        price: 4500,
        sold_count: 3
      }
    ];
  }
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
}
