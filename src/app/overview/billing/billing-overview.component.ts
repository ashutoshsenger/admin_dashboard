import { Component, ViewContainerRef, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from '../../services/loader.service';
import 'rxjs/add/observable/of';
import { ActivatedRoute } from '@angular/router';
import {BaseChartDirective} from 'ng2-charts/ng2-charts';

@Component({
  selector: 'billing',
  templateUrl: './billing-overview.component.html',
  styleUrls: ['./billing-overview.component.css']
})

export class BillingOverviewComponent implements OnInit {

  constructor(
    private loaderService: LoaderService,
    public dialog: MatDialog,
    public viewContainerRef_bookings: ViewContainerRef,
    private route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) { }
  
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  public date = new Date().toISOString().slice(0, 10);

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
 
  public total_billing =  1300;
  public not_paid_amount =  900;
  public paid_amount =  400;
 
  public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
  
  ngOnInit() {
  }

}
