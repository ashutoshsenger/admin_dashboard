import { Component, ViewContainerRef, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from '../services/loader.service';
import 'rxjs/add/observable/of';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})

export class OverviewComponent implements OnInit {

  constructor(
    private loaderService: LoaderService,
    public dialog: MatDialog,
    public viewContainerRef_bookings: ViewContainerRef,
    private route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) { }

    public date = new Date().toISOString().slice(0, 10);

  ngOnInit() {
  }
}
