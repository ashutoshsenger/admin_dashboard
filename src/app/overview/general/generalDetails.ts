export class GeneralDetails {
  occupancy_final_details: {
    separate_occupancy_details: Object;
    average_data: number;
  };
  revenue_final_details: {
    total_revenue: number;
    target_revenue: number;
  };
  invoice_final_details: {
    total_count: number;
    overdue_count: number;
    paid_count: number;
  };
  member_final_details: {
    active_count: number;
    new_count: number;
  };
  company_final_details: {
    active_count: number;
    new_count: number;
  }
}

export class ResourceDetails {
  occupancy: number;
  resources_count: number;
  occupancy_percent: number;
}
