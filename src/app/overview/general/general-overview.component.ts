import { Component, ViewContainerRef, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import { AppGlobals } from '../../services/app-globals';
import { DataSource } from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from '../../services/loader.service';
import { GeneralDetails, ResourceDetails } from './generalDetails';
import { OverviewService } from '../../services/overview.service';
import 'rxjs/add/observable/of';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'general',
  templateUrl: './general-overview.component.html',
  styleUrls: ['./general-overview.component.css']
})

export class GeneralOverviewComponent implements OnInit {

  public occupancy_final_details : {average_data: number; separate_occupancy_details: Object};
  public separate_occupancy_details;
  public invoice_final_details;
  public member_final_details;
  public company_final_details;
  public revenue_final_details;
  public hotdesk_details: ResourceDetails;
  public dedicated_details: ResourceDetails;
  public cabin_details: ResourceDetails;

  constructor (
    private loaderService: LoaderService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private _appGlobals: AppGlobals,
    private _overviewService: OverviewService
  ) {
    this.dedicated_details = new ResourceDetails;
    this.hotdesk_details= new ResourceDetails;
    this.cabin_details= new ResourceDetails;

    this.occupancy_final_details = {
      average_data: 0,
      separate_occupancy_details: {}
    };
    this.separate_occupancy_details = {};
    this.company_final_details = {
      active_count: 0,
      new_count: 0
    };

    this.member_final_details = {
      active_count: 0,
      new_count: 0
    };

    this.invoice_final_details = {
      paid_amount: 0,
      total_amount: 0,
      overdue_amount: 0
    };

    this.revenue_final_details = {
      target_revenue: 0,
      total_revenue: 0
    }
  }

  ngOnInit() {
    this._appGlobals.selectedLocationId.subscribe(id => {
      if (id || id == 0) {
        this._populateGneralDetails();
      }
    })
  }

  private _populateGneralDetails() {
    this._overviewService.getGeneralDetails()
      .then(res => {
        let details = JSON.parse(JSON.stringify(res.data));
        this.occupancy_final_details = details.occupancy_final_details;
        this.separate_occupancy_details = this.occupancy_final_details.separate_occupancy_details;
        let category_occupancy_details = this.separate_occupancy_details;
        if (category_occupancy_details) {
          this.hotdesk_details= category_occupancy_details[4] || {};
          this.dedicated_details = category_occupancy_details[1] || {};
          this.cabin_details = category_occupancy_details[3] || {};
        }
        this.invoice_final_details = details.invoice_final_details;
        this.revenue_final_details = details.revenue_final_details;
        this.member_final_details = details.member_final_details;
        this.company_final_details = details.company_final_details;
      })
      .catch(error => {
        console.log(error);
      })
  }
}
