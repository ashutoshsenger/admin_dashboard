import { Component, ViewContainerRef, OnInit, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import {Parser} from 'json2csv';
import { AppGlobals } from '../../services/app-globals';
import {MatSnackBar, MatTableDataSource} from '@angular/material';
import {RevenueDetails} from './revenueDetails';
import {RevenueService} from '../../services/revenue.service';
import * as moment from 'moment';
import * as _ from 'lodash';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/observable/of';
// import {BaseChartDirective} from 'ng2-charts/ng2-charts';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'revenue',
  templateUrl: './revenue-overview.component.html',
  styleUrls: ['./revenue-overview.component.css']
})

export class RevenueOverviewComponent implements OnInit {

  public revenueDataSource;
  public list_locations;
  public final_array = [];

  constructor(
    private route: ActivatedRoute,
    public snackBar: MatSnackBar,
    public revenueService: RevenueService,
    private _appGlobals: AppGlobals
  ) {
    this._appGlobals.locationsArray.subscribe(locations => this.list_locations = locations);
  }

  private month_name_1;
  private month_name_2;
  private month_name_3; 
  // @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  public date = new Date().toISOString().slice(0, 10);

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public columns: Array<any>; 
  public displayedColumns: string[]; 
  /*public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];

  public lineChartData:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Cash Occupancy'}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];*/

  ngOnInit() {
    this.month_name_1 = moment().format('MMM');
    this.month_name_2 = moment().subtract(1, 'month').format('MMM');
    this.month_name_3 = moment().subtract(2, 'month').format('MMM');

    this.columns = [
      { name: 'location', label: 'Location'},
      { name: this.month_name_3, label: this.month_name_3},
      { name: this.month_name_2, label: this.month_name_2},
      { name: this.month_name_1, label: this.month_name_1}
    ];

    this.displayedColumns = this.columns.map(column => column.name);
    this._fetchMonthlyData();
  }

  private _fetchMonthlyData() {
    let month_year_1 = moment().format('YYYYMM');
    let month_year_2 = moment().subtract(1, 'month').format('YYYYMM');
    let month_year_3 = moment().subtract(2, 'month').format('YYYYMM');

    this.revenueService.getGeneralDetails(month_year_1, month_year_2, month_year_3)
    .then(res => {
      if (res.success) {
        let revenue_details_array = Object.assign([], res.data);
        let selected_data;
        let final_array_obj = {};

        this.list_locations.forEach(location => {
          selected_data = _.find(revenue_details_array, revenue_item => revenue_item.id == location.id) || {};
          final_array_obj = {};
          if (Object.keys(selected_data).length) {
            final_array_obj = Object.assign({}, selected_data);
          } else {
            final_array_obj['location'] = location.name;
            final_array_obj[this.month_name_3] = 0;
            final_array_obj[this.month_name_2] = 0;
            final_array_obj[this.month_name_1] = 0;
          }
          this.final_array.push(final_array_obj);
        });
        this.revenueDataSource = new MatTableDataSource(this.final_array);
      } else {
        this._openSnackBar ('Error', res.message);
      }
    })
    .catch(error => {
      console.log(error);
    })
  };

  public downloadAsCSV () {
    let data = Object.assign([], this.final_array);
    const fields = ['location', this.month_name_3, this.month_name_2, this.month_name_1];
    const json2csvParser = new Parser({ fields });
    const csv = json2csvParser.parse(data);
    let blob = new Blob([csv], { type: 'text/csv' });
    let dataURL = window.URL.createObjectURL(blob);
     const link = document.createElement('a');
     link.href = dataURL;
     link.download = 'RevenueData.csv';
     link.click();
     this._openSnackBar("Downloaded successfully", "Success");
  }

  private _openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
 
 // public lineChartLegend:boolean = true;
 // public lineChartType:string = 'line';

 // // events
 // public chartClicked(e:any):void {
 //   console.log(e);
 // }
 //
 // public chartHovered(e:any):void {
 //   console.log(e);
 // }
}
