import { Component, ViewContainerRef, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from '../../services/loader.service';
import 'rxjs/add/observable/of';
import { ActivatedRoute } from '@angular/router';
import {BaseChartDirective} from 'ng2-charts/ng2-charts';

@Component({
  selector: 'occupancy',
  templateUrl: './occupancy-overview.component.html',
  styleUrls: ['./occupancy-overview.component.css']
})

export class OccupancyOverviewComponent implements OnInit {

  constructor(
    private loaderService: LoaderService,
    public dialog: MatDialog,
    public viewContainerRef_bookings: ViewContainerRef,
    private route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) { }
  
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  public date = new Date().toISOString().slice(0, 10);
    
  public lineChartData:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Occupancy'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Dedicated Desk'},
    {data: [18, 48, 77, 9, 100, 27, 40], label: 'Hot desk'},
    {data: [18, 9, 67, 9, 100, 27, 10], label: 'Private Office'}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
 
  ngOnInit() {
  }

  
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
   
}
