import { Component, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef, MatSnackBar, MatSort, MatTableDataSource, MatOption} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { AppGlobals } from '../services/app-globals';
import { LoaderService } from '../services/loader.service';
import { TransactionService } from '../services/transaction.service';
import {GlobalVariables} from '../global/global-variables';
import * as moment from 'moment';
import * as _ from 'lodash';
import * as FileSaver from 'file-saver';

export interface queries {
  s_no: number;
  alternate_id: string;
  entity_alternate_id: string;
  total_amount: number;
  entity_name: string;
  entity_type: number;
  status: number;
  paid_status: number;
  send_status: number;
  tracked_with_bhaifi: number;
}

@Component({
  selector: 'app-queries',
  templateUrl: './queries.component.html',
  styleUrls: ['./queries.component.css']
})
export class QueriesComponent implements OnInit {

  public selected_status: number;
  public selected_queries: number;
  public list_queries;
  public search_string;
  public date_display;
  public date;
  public queries_details;
  constructor(
    private loaderService: LoaderService,
    private _querieservice: TransactionService,
    public dialog: MatDialog,
    public viewContainerRef: ViewContainerRef,
    public confirm_dialog: MatDialog,
    public confirm_viewContainerRef: ViewContainerRef,
    private _appGlobals: AppGlobals,
    public snackBar: MatSnackBar
  ) {}

  dialogRef: MatDialogRef<any>
  confirm_dialogRef: MatDialogRef<any>

  private _queries_data: queries[];
  public selected_queries_data: queries[] = [];
  public queriesDataSource;

  selection = new SelectionModel<queries>(true, []);
  queriesDisplayedColumns: string[] = ['id', 'amount', 'status', 'gateway_id', 'coupon_code', 'added_at', 'visits', 'purpose' ];

  @ViewChild('allSelected') private allSelected: MatOption;

  @ViewChild(MatSort) sort: MatSort;
  ngOnInit() {
    this.selected_status = 0;
    this.populatequeries();
    
  }

  public populatequeries () {
    this._querieservice.getAll()
    .then(res => {
      if (res.success) {
        this._queries_data = Object.assign([], res.data);
        this.selected_queries_data = this._queries_data;
        this.queriesDataSource = new MatTableDataSource(this.selected_queries_data)
        this.queriesDataSource.sort = this.sort;
      } else {
        alert(res.message || "Some error occured");
      }
    })
    .catch(error=> {
      console.log(error);
    })
  }

  public changeStatus(status: number) {
    this.selected_status = status;
    switch (status) {
      case 0:
        this.selected_queries_data = Object.assign([], this._queries_data);
        break;
      case 1:
        this.selected_queries_data = Object.assign([], _.filter(this._queries_data, ['status', 1]));
        break;
      case 2:
        this.selected_queries_data = Object.assign([], _.filter(this._queries_data, ['status', 2]));
        break;
      default:
        this.selected_queries_data = Object.assign([], this._queries_data);
    }
      this.queriesDataSource = new MatTableDataSource(this.selected_queries_data)
  }

  private _openSnackBar(message: string, action: string) {
    this.loaderService.displayLoader(false);
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

  applyFilter(filterValue: string) {
    this.queriesDataSource.filter = filterValue.trim().toLowerCase();
  }

  public clearSearchInput () {
    this.search_string = '';
    this.applyFilter('');
  }

}