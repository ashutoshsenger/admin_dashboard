import {Component} from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material';
import { AppGlobals } from '../services/app-globals';
import { Router } from '@angular/router';

@Component({
  selector: 'location-list-bottom-sheet',
  templateUrl: './location-list-bottom-sheet.component.html',
  styleUrls: ['./location-list-bottom-sheet.component.css']
})
export class LocationListBottomSheet {

  public list_locations;
  public list_all_locations;

  constructor(
    private bottomSheetRef: MatBottomSheetRef<LocationListBottomSheet>,
    private _appGlobals: AppGlobals,
    private router: Router
  ) {
    this._appGlobals.locationsArray.subscribe(locations => this.list_locations = locations);
    this._appGlobals.allLocationsArray.subscribe(locations => this.list_all_locations = locations);
  }

  ngOnInit () {
    this.list_locations = this.list_locations.filter(item => item.id != 0);
    if (this.list_locations.length == this.list_all_locations.length) {
      this.list_locations.push({id: 0, name: "All Locations"});
    }
  }

  openLink(location_id: number, location_name: string, event: MouseEvent): void {
    this._appGlobals.setLocationId(location_id);
    localStorage.setItem('location_id', ""+location_id);
    localStorage.setItem('location_name', ""+location_name);
    this.bottomSheetRef.dismiss();
    localStorage.setItem('saved_url', ""+this.router.url);
    location.reload();
    event.preventDefault();
  }
}
