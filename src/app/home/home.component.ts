import { Component, ViewContainerRef, OnInit } from '@angular/core';
import { LoaderService } from '../services/loader.service';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material';
import {LocationListBottomSheet} from './location-list-bottom-sheet.component';
import {AccountListBottomSheet} from './account-list-bottom-sheet.component';
import { AppGlobals } from '../services/app-globals';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import { Router } from '@angular/router';
import { mapTo } from 'rxjs/operators';
import { Observable, fromEvent, merge, of } from 'rxjs';
import { ManagerService } from '../services/manager.service';
import * as _ from 'lodash';
import { LogoutDialog } from './logout-dialog.component';
import { AccountSuspendedDialog } from './accountSuspended-dialog.component';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  public space_name;
  public list_accounts;

  public modules_access = {
    community: true,
    overview: true,
    billing: true,
    issues: true,
    bookings: true,
    conversations: true,
    settings: true
  };

  public objLoaderStatus: boolean = false;
  public dialogRef: MatDialogRef<any>;
  public selected_location_name: string;
  public is_no_internet_bar_hidden: boolean;
  public online$: Observable<boolean>;

  constructor(
    private router: Router,
    private bottomSheet: MatBottomSheet,
    private loaderService: LoaderService,
    public dialog: MatDialog,
    private _appGlobals: AppGlobals,
    private viewContainerRef: ViewContainerRef,
    private managerService: ManagerService
  ) {
    this.online$ = merge(
      of(navigator.onLine),
      fromEvent(window, 'online').pipe(mapTo(true)),
      fromEvent(window, 'offline').pipe(mapTo(false))
    );

    this.online$.subscribe(value => {
      this.is_no_internet_bar_hidden = value;
    })
    this.list_accounts = [];
  }

  public overview_panel_open_state = true;
  public community_panel_open_state = false;
  public billing_panel_open_state = false;
  public space_locations;
  public space_details;
  public location_ids;
  public list_locations;
  public selectedLocationId;

 

  ngOnChanges () {
  
  }

  ngOnInit() {
   // window.addEventListener('online', () => {
   //   this.is_no_internet_bar_hidden = true;
   // });
   // window.addEventListener('offline', () => {
   //   this.is_no_internet_bar_hidden = false;
   // })
    this.loaderService.loaderStatus.subscribe((val: boolean) => {
      this.objLoaderStatus = val;
    });
  }

  private _setLocationNameAndId () {
    let default_location_name = "All Locations";
    let default_location_id = 0;
    let stored_location_name = localStorage.getItem('location_name');
    let stored_location_id = localStorage.getItem('location_id');
    if (this.list_locations.length != this.space_locations.length) {
      default_location_name = this.list_locations[0].name;
      default_location_id = this.list_locations[0].id;
    }
    this.selected_location_name =  stored_location_name || default_location_name;
    this._appGlobals.setLocationId(stored_location_id || default_location_id);
  }

  private _setAccessibleLocations () {
    if (this.location_ids) {
      let final_locations = _.filter(this.space_locations, (location) => { return this.location_ids.indexOf(location.id) >= 0 });
      this._appGlobals.setLocationsArray(final_locations);
      this.list_locations = Object.assign([], final_locations);
    } else {
      this.list_locations = Object.assign([], this.space_locations);
      this._appGlobals.setLocationsArray(this.space_locations);
    }
  }

  private _routeToRelevantSection () {
    let saved_url = localStorage.getItem('saved_url');
    if (saved_url) {
      this.router.navigate([''+saved_url]);
      localStorage.removeItem('saved_url');
    } else {
      let modules_array = Object.keys(this.modules_access);
      if (modules_array.length < 7) {
        this.router.navigate(['home/' + modules_array[0]]);
      }
    }
  }

  openLocationBottomSheet(): void {
    this.bottomSheet.open(LocationListBottomSheet);
  }

  openAccountBottomSheet(): void {
    this.bottomSheet.open(AccountListBottomSheet);
  }

  openLogoutDialog () {
    let config = new MatDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    config.disableClose = true;
    this.dialogRef = this.dialog.open(LogoutDialog, config);
    this.dialogRef.afterClosed().subscribe(result => {
      result.success ? this.router.navigate(['login']) :"";
      this.dialogRef = null;
    });
  }

  openAccountSuspendedDialog() {
    let config = new MatDialogConfig();
    config.disableClose = true;
    config.viewContainerRef = this.viewContainerRef;
    this.dialogRef = this.dialog.open(AccountSuspendedDialog, config);
  }
}
