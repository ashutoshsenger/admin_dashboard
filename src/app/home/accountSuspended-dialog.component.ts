import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'accountSuspended',
  templateUrl: './accountSuspended-dialog.component.html',
  styleUrls: ['./accountSuspended-dialog.component.css']
})
export class AccountSuspendedDialog {

  constructor(
    public dialogRef: MatDialogRef<any>
  ) {}
}
