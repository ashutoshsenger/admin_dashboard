import {Component} from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material';

import { ManagerService } from '../services/manager.service';
import { AppGlobals } from '../services/app-globals';
import { Router } from '@angular/router';

@Component({
  selector: 'account-list-bottom-sheet',
  templateUrl: './account-list-bottom-sheet.component.html',
  styleUrls: ['./account-list-bottom-sheet.component.css']
})
export class AccountListBottomSheet {

  public list_accounts;

  public selected_account_id;
  constructor(
    private bottomSheetRef: MatBottomSheetRef<AccountListBottomSheet>,
    private _appGlobals: AppGlobals,
    private _managerService: ManagerService,
    private router: Router
  ) {
    this._appGlobals.accountsArray.subscribe(accounts => this.list_accounts = accounts || []);
  }

  ngOnInit () {
    this.selected_account_id = localStorage.getItem('account_id') || 1;
  }

  openLink(account_id: number, account_name: string, event: MouseEvent): void {
    this._managerService.setSelectedAccountCookie(account_id)
    .then(data => {
      if (data.success) {
        localStorage.setItem('account_id', ""+account_id);
        this.bottomSheetRef.dismiss();
        location.reload();
        event.preventDefault();
      }
    })
    .catch (error => {
      console.log(error);
    })
  }
}
